﻿<#Script by Sanoj Rajan,
  sanojrs@gmail.com, to get documents from a SharePoint 2013 farm and to add the document name and URL to an existing list.#>

##Required parameters :
###SiteCollectionUrl - URL of the site collection. eg: http://abc.com
###DestinationListUrl - URL of the list in which the document url's are going to be added. eg: http://abc.com/Lists/TestList/AllItems.aspx

param (
    [string]$SiteCollectionUrl = "$(Read-Host 'Enter the Site Collection URL.')",
    [string]$DestinationListUrl = "$(Read-Host 'Enter the Destination List URL.')"
)

If ((Get-PSSnapIn -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )  
{ Add-PSSnapIn -Name Microsoft.SharePoint.PowerShell } 


##Function to get documents from all the sites
function Get-DocumenDetails() {

try
{
    #Get all sites
    $siteCollection = Get-SPSite $SiteCollectionUrl
    $webSites = $siteCollection.AllWebs

    ##Get reference to destination list and add new field to store document URL
    $DestinationSite = New-Object Microsoft.SharePoint.SPSite($DestinationListUrl)
    $DestinationWeb = $DestinationSite.OpenWeb()

    #Refer destination list
    $DestinationList =  $DestinationWeb.GetListFromUrl($DestinationListUrl)

    #Delete items from list, if there exists any data. This will help to remove duplicate entries if we run the script multiple times
    $ListItems = $DestinationList.Items

    foreach ($ListItem in $ListItems)
    {
     $DestinationList.GetItemById($ListItem.id).Delete()
    }

    #Set field type and name 
    $FieldType = [Microsoft.SharePoint.SPFieldType]::Text
    $FieldName="DocumentURL"
    $IsRequired = $False

    #Checks if field to store document url already exists in the list, If not add the field
    if(!$DestinationList.Fields.ContainsField($FieldName))
    {     
      #Add column to the List
      $DestinationList.Fields.Add($FieldName,$FieldType,$IsRequired)
 
      #Update the List
      $DestinationList.Update()
 
      #Update the default view to include the new column
      $View = $DestinationList.DefaultView
      $View.ViewFields.Add($FieldName)
      $View.Update()
 
      write-host "New Column '$FieldName' Added to the List!" -ForegroundColor Green
      }
      else
      {
       write-host "Field '$FieldName' Already Exists in the List" -ForegroundColor Red
      }    
    

    #######################Get Document Details#################

    ##Iterate through all web sites 
    foreach ($webSite in $webSites.Webs)
    {
        $doccount = 0
        #Iterate through each list
        foreach ($list in $webSite.Lists) 
        {
            if($list.BaseTemplate -eq "DocumentLibrary")             
            {
                ##Filter list item where the field "Confidential" is set to "Yes"
                $listItems = $list.Items | ?{$_['Confidential'] -eq $TRUE}

                foreach ($item in $listItems)
                {
                 ##Add the document name and document url to the destination list  
                 $NewItem = $DestinationList.Items.Add()
                 $NewItem["Title"] = $item.Name
                 $NewItem["DocumentURL"] = $webSite.Url + "/" + $item.Url.Replace(" ", "%20") 
                 $NewItem.Update()

                 write-host "Document "  $item.Name " added to list."
                }
             
            }
        }
     }

    $DestinationWeb.Dispose()
    $DestinationSite.Dispose()
    $webSites.Dispose(); 
    $siteCollection.Dispose()

    }
    catch 
    {
        Write-Host $_.Exception.Message -ForegroundColor Red
    }
}

##Call function to get the document details
Get-DocumenDetails 

Remove-PSSnapin Microsoft.SharePoint.PowerShell